import json
import os
import shutil
from typing import Any, Dict, List


def initialize_metadata(file_path: str) -> Dict[str, Any]:
    """Initializes a dictionary to store metadata.

    This function creates a dictionary intended to store metadata, with the two keys
    `all` and `setup_path`. The value of the entry `all` is itself a dictionary
    containing the keys `values` and `names`, the corresponding values of the dictionary
    are empty lists. The path to the setup file is stored with the key `setup_path` in
    the dictionary. The dictionary structure is shown in the example below.

    Args:
        file_path (str): File path to the JSON setup file.

    Returns:
        Dict[str, Any]: A dictionary with the metadata.

    Example:
        The returned dictionary should have the following sturcture:
        {
            "all": {
                "values": [], # A list of all UUIDs.
                "names": [], # A list of all names.
            },
            "setup_path": "/path/to/setup_up.json",
        }

    """
    metadata = {"all": {"values": [], "names": []},"setup_path": file_path}
    #Erstellt ein Dictionary namens "metadata", in welchem sich ein weiteres Dictionary befindet welches die UUIDs und die dazu passenden Namen speichern soll. Zudem wird der Dateipfad für "setup_path" gespeichert.
    
    return metadata
    #Das dict "metadata" wird ausgegeben.

def add_metadata(file_path: str, metadata: Dict[str, Any]) -> Dict[str, Any]:
    """Appends components UUIDs and corresponding names from a JSON setup file to
    provided the metadata dictionary.

    The UUIDs are stored in the list which is accessed with the two keys `all` and
    `values`, the names are stored in the list which is accessed with the keys `all` and
    `names`.

    To do so, the function reads a JSON setup file located at the given path and
    iterates over all UUIDs. It appends each UUID to the list with the key `values`, and
    its associated name to the list with the key `names`, both of which can be found
    under the key `all` in the metadata dictionary. The dictionary structure is shown in
    the example below.

    Args:
        file_path (str): Path to the JSON setup file.
        metadata (Dict[str, Any]): A dictionary with the metadata.

    Returns:
        Dict[str, Any]: A dictionary with the metadata, filled with UUIDs and component
            names.

    Example:
        The returned dictionary should have the following sturcture:
        {
            "all": {
                "values": ["uuid_1", "uuid_2", ...], # A list of all UUIDs.
                "names": ["name_1", "name_2", ...], # A list of all names.
            },
            "setup_path": "/path/to/setup_up.json",
        }

    """
    setup_file = get_json_entry_by_path(file_path, ["setup"])
    #Speichern der Einträge der JSON Datei unter "setup" in Variable "setup_file".
    uuid_list = list(setup_file.keys())
    #Die keys von "setup_file", die UUIDs, werden in uuid_list gespeichert.
    number_of_uuids = len(uuid_list)
    #Speichern der Anzahl an UUIDs in uuid_list in number_of_uuids.       
    
    name_list = []
    #Erstellen der noch leeren Liste name_list.
    i = 0
    #Zählzeit i wird gleich 0 gesetzt.
    
    while i < number_of_uuids:
        uuid = setup_file[uuid_list[i]]
        name = uuid["name"]
        name_list.append(name)
        i = i + 1
    #Für jede UUID in uuid_list wird der zugehörige Name in name_list ghespeichert.
    
    metadata = {"all": {"values": uuid_list, "names": name_list},"setup_path": file_path}
    #Dict metadata wird mit den values uuid_list und names name_list geupdatet.
    
    return metadata
    #Ausgabe von metadata.

def add_metadata_for_types(file_path: str, metadata: Dict[str, Any]) -> Dict[str, Any]:
    """Adds new dictionaries to the metadata dictionary containing the UUIDs and names
    of only components of certain types from the JSON setup file.

    This function extracts unique types from a JSON setup file and updates the metadata
    dictionary. It adds new keys called the same as the types, associating them with a
    dictionary containing the keys `values` and `names`, which store lists of names and
    UUIDs corresponding to that type. The dictionary structure is shown in the example
    below.

    Args:
        path (str): Path to a JSON setup file.
        metadata (Dict[str, Any]): A dictionary with the metadata.

    Returns:
        Dict[str, Any]: A dictionary with the metadata, filled with dictionaries for
            different types of components.

    Example:
        The returned dictionary should have the following sturcture:
        {
            "all": {
                "values": ["uuid_1", "uuid_2", ...],
                "names": ["name_1", "name_2", ...],
            },
            "setup_path": "/path/to/setup_up.json",
            "sensor": {
                "values": ["uuid_1", ...], # A list of all UUIDs with `sensor` type.
                "names": ["name_1", ...], # A list of all names with `sensor` type.
            },
            "type_1": {
                "values": ["uuid_2", ...], # A list of all UUIDs with type_1.
                "names": ["name_2", ...], # A list of all names with type_1.
            },
            ...
        }

    """
    setup_file = get_json_entry_by_path(file_path, ["setup"])
    #Speichern der Einträge der JSON Datei unter "setup" in Variable "setup_file".
    uuid_list = list(setup_file.keys())
    #Die keys von "setup_file", die UUIDs, werden in uuid_list gespeichert.
    number_of_uuids = len(uuid_list)
    #Speichern der Anzahl an UUIDs in uuid_list in number_of_uuids. 
    
    a=0
    #Zählzeit a = 0.
    
    for i in uuid_list:
        uuid = setup_file[uuid_list[a]]
        #Jede UUID aus uuid_list wird in uuid gespeichert.
        typ = uuid["type"]
        name = uuid["name"]
        #Der jeweilige type und name zur uuid wird gespeichert in typ und name.
        uuid_entry = uuid_list[a]
        #UUID wird in uuid_entry gespeichert.
        if not typ or not uuid or not name:
            continue
        #Inhalte die unter den UUIDs hinterlegt sind, aber weder name noch type sind werden übergangen.
        if typ not in metadata:
            metadata[typ] = {"values":[], "names":[]}
        #Speichern der types in metadata in dict als key für values value und names.
        metadata[typ]["values"].append(uuid_entry)
        #Die UUID wird den values vom key typ hinzugefügt.
        metadata[typ]["names"].append(name)
        #Der Name wird den names vom key typ hinzugefügt.
        a = a+1
        #Die Zählzeit wird um 1 erhöht.
   
    return metadata
    #Das geupdatet dict metadata wird ausgegeben.
    


def add_temperature_sensor_serials(
    folder_path: str, metadata: Dict[str, Any]
) -> Dict[str, Any]:
    """Adds the serial number of temperature sensors to the provided metadata
    dictionary.

    This function updates the given dictionary by adding a list of sensor serial numbers
    extracted from JSON data sheets located in the specified folder path. The serial
    numbers are added to the dictionary located at the `sensor` key. There, for the key
    `serials`, a new key-value pair is created, see the example below.

    Args:
        folder_path (str): Path to the folder containing temperature sensor data sheets.
        metadata (Dict[str, Any]): The metadata dictionary to be updated. The dictionary
            is expected to have a `sensor` key with a nested `values` key containing
            UUIDs.

    Returns:
        Dict[str, Any]: The updated metadata dictionary.

    Example:
        The returned dictionary should have the following sturcture:
        {
            "all": {
                "values": ["uuid_1", "uuid_2", ...],
                "names": ["name_1", "name_2", ...],
            },
            "setup_path": "/path/to/setup_up.json",
            # Only for sensor type, there is serials key.
            "sensor": {
                "values": ["uuid_1", ...],
                "names": ["name_1", ...],
                "serials": ["serial_1", ...], # A list of all serials of sensor
            },
            "type_1": {
                "values": ["uuid_2", ...],
                "names": ["name_2", ...],
            },
            ...
        }

    """
    l1 = get_json_entry_by_uuid(folder_path, "1efa1b5d-ab3f-67cf-9568-314724ae7a79", ["sensor","serial"])
    l2 = get_json_entry_by_uuid(folder_path, "1efa1b67-d47d-68b1-a923-2c37ef35477a", ["sensor","serial"])
    m1 = get_json_entry_by_uuid(folder_path, "1efa1b5d-ab3f-67cf-9568-314724ae7a79", ["sensor","name"])
    m2 = get_json_entry_by_uuid(folder_path, "1efa1b67-d47d-68b1-a923-2c37ef35477a", ["sensor","name"])
    #Mit Hilfe der get_json_entry_by_uuid Funktion werden die Namen und Seriennummern der Sensoren ausgelesen und gespeichert in Variablen l1, l2, m1 und m2.
    metadata["sensor"] = {"values": ["1efa1b5d-ab3f-67cf-9568-314724ae7a79", "1efa1b67-d47d-68b1-a923-2c37ef35477a"], "names": [m1, m2], "serials" : [l1, l2]}
    #Dem Type sensor der in metadata gespeichert ist werden die UUIDs als values, die Sensornamen als names und die Seriennummern als serials übergeben.
    
    return metadata
    #Das geupdatet dict metadata wir ausgegeben.

def get_json_entry_by_uuid(
    folder_path: str, uuid: str, json_intern_path: List[str]
) -> Any:
    """Searches for a specific JSON value associated with the given UUID in a directory
    structure.

    Args:
        folder_path (str): Path to the folder containing JSON files and subfolders.
        uuid (str): UUID to be searched for within the JSON files.
        json_intern_path (List[str]): A list of keys representing the internal path to
            the desired value within a JSON file.

    Returns:
        Any: JSON value.

    """
    file_path = _get_unique_json_path(folder_path, uuid)
    json_entry = get_json_entry_by_path(file_path, json_intern_path)
    return json_entry


def get_json_entry_by_path(file_path: str, json_intern_path: List[str]) -> Any:
    """Searches for a specific JSON value in a given JSON file.

    Args:
        file_path (str): Path to the JSON file.
        json_intern_path (List[str]): A list of keys representing the path to the
            desired value within the JSON file.

    Returns:
        Any: Value from JSON file.

    """
    with open(file_path, "r") as json_file:
        json_entry = json.load(json_file)
    try:
        for jp in json_intern_path:
            json_entry = json_entry[jp]
        return json_entry
    except KeyError:
        print(
            "[ERROR] There is no entry ({}) in the json file ({})".format(
                "->".join(json_intern_path), file_path
            )
        )
        raise


def get_metadata_from_setup(file_path: str) -> Dict[str, Any]:
    """Extracts IDs, names, and types from a JSON setup file.

    This function reads setup file with a specific structure and extracts UUIDs, names,
    and types.

    Args:
        file_path (str): File path to a setup file.

    Returns:
        Dict[str, Any]: Metadata dictionary.

    """
    metadata = initialize_metadata(file_path)
    metadata = add_metadata(file_path, metadata)
    metadata = add_metadata_for_types(file_path, metadata)

    return metadata


def archiv_json(folder_path: str, setup_path: str, archiv_path: str) -> None:
    """Archives matching data sheets from a given folder based on setup data sheet.

    This function walks through the directory structure starting from the `folder_path`,
    looking for JSON files that match specific UUIDs defined in a setup file
    (`setup_path`). Any matching files are copied to the `archiv_path` directory, and
    their filenames are modified to include their UUIDs. The function expects the JSON
    files to have a structure where they contain a `JSON` key with a nested `ID` key. If
    a JSON file does not have this structure, a warning is printed, and the file is
    skipped.

    Args:
        folder_path (str): The path to the root folder containing data sheets and
            subfolders to search.
        setup_path (str): The path to the setup data sheet file that contains the UUIDs
            to match against.
        archiv_path (str): The path to the folder where matching JSON files should be
            archived. If the directory does not exist, it will be created.

    """
    setup_data = get_json_entry_by_path(setup_path, ["setup"])

    if not os.path.exists(archiv_path):
        os.makedirs(archiv_path)

    for uuid in tuple(setup_data.keys()):
        try:
            file_path = _get_unique_json_path(folder_path, uuid)
        except ValueError:
            print("[ERROR] Can not archiv file(s) with UUID: {}".format(uuid))
            continue
        copy_name = "{}_{}.json".format(
            os.path.basename(file_path).split(".json")[0], uuid
        )
        shutil.copyfile(file_path, os.path.join(archiv_path, copy_name))

    shutil.copyfile(setup_path, os.path.join(archiv_path, os.path.basename(setup_path)))


def _get_unique_json_path(folder_path: str, uuid: str) -> str:
    """Get a unique JSON file path by UUID.

    Args:
        folder_path (str): Path to the folder containing JSON files and subfolders.
        uuid (str): UUID to search for.

    Raises:
        ValueError: No JSON file found with the given UUID.
        ValueError: Multiple JSON files found with the given UUID.

    Returns:
        str: Path to the JSON file with the given UUID.

    """
    uuid_path = _get_json_file_path_by_uuid(folder_path, uuid)
    if len(uuid_path) == 1:
        return uuid_path[0]
    elif len(uuid_path) < 1:
        raise ValueError("[ERROR] No path is found under the uuid: {}".format(uuid))
    else:
        raise ValueError(
            "[ERROR] The uuid {} exists in more than one file: {}".format(
                uuid, uuid_path
            )
        )


def _get_json_file_path_by_uuid(folder_path: str, uuid: str) -> List[str]:
    """Recursively searches for JSON files with given uuid.

    Args:
        folder_path (str): Path to JSON files folder.
        uuid (str): UUID to search for.

    Returns:
        List[str]: Paths of the JSON files with the given UUID.

    """
    uuid_path = []
    folder_content = os.listdir(folder_path)
    for i in folder_content:
        path = os.path.join(folder_path, i)
        if os.path.isfile(path) and i.endswith(".json"):
            try:
                json_uuid = get_json_entry_by_path(path, ["JSON", "ID"])
            except KeyError:
                print("[ERROR] JSON-ID not found, {} skip...".format(path))
                continue
            if json_uuid == uuid:
                uuid_path.append(path)

        elif os.path.isdir(path):
            uuid_path.extend(_get_json_file_path_by_uuid(path, uuid))
    return uuid_path
