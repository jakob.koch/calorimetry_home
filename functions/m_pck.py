import json
import os
import time
from typing import Any, Dict, List

import h5py as h5
import numpy as np
from w1thermsensor import Sensor, W1ThermSensor
from w1thermsensor.errors import SensorNotReadyError

from functions import m_json

def check_sensors() -> None:
    """Retrieves and prints the serial number and current temperature of all DS18B20
    Temperature Sensors.

    This function utilizes the `w1thermsensor` library to interact with the DS18B20
    temperature sensors. For each available sensor, it prints its serial number and the
    current temperature reading.

    Examples:
    Assuming two DS18B20 sensors are connected.
    >>> check_sensors()
    Sensor 000005888445 has temperature 25.12
    Sensor 000005888446 has temperature 24.89

    """
    for sensor in W1ThermSensor.get_available_sensors([Sensor.DS18B20]):
        print("Sensor %s has temperature %.2f" % (sensor.id, sensor.get_temperature()))
    #Funktion gibt für alle angeschlossenen Sensoren die Temperatur und Seriennummer aus.

def add_experiment_data(
    sensor: W1ThermSensor,
    uuid: str,
    data: Dict[str, List[List]],
    timestamp: float,
) -> Dict[str, List[List]]:
    """Saves the measurement data returned by the sensor object in a data structure
    with a timestamp.

    The function calls another function to access the corresponding sensor and its
    temperature. The temperature is stored in the data structure together with a
    timestamp. The timestamp and the data structure are input arguments of the
    function. The data structure consists of a dictionary, with UUIDs of sensors
    as keys and a nested list as values, see the example below.

    Args:
        sensor (W1ThermSensor): Sensor to be accessed.
        uuid (str): UUID of the corresponding sensor.
        data (Dict[str, List[List]]): Data structure.
        timestamp (float): A timestamp, measured relative to the start of
            the experiment.

    Returns:
        Dict[str, List[List]]: The same data structure with new measurement data.

    Example:
        A Example of the data structure
        {
            "uuid_1": [
                [22, 25, 28], # A list of temperatures
                [0, 3, 6], # A list of timestamps
            ],
            "uuid_2": [
                [22, 25, 28],
                [2, 4, 7],
            ],
        }

    """
    temp = get_temperature(sensor)
    #Auslesen des Sensors mittels get_temperature und speichern in Variable temp.
    if uuid == "1efa1b5d-ab3f-67cf-9568-314724ae7a79":
    #Ist die gegebene uuid gleich der von Thermometer 1 wird das Folgende ausgeführt:    
        dict_value_of_uuid1 = data["1efa1b5d-ab3f-67cf-9568-314724ae7a79"]
        #Dictionary für UUID von Thermometer 1 wird in dict_value_of_uuid1 gespeichert.
        dict_value_of_uuid1[0].append(temp)
        #In die erste Liste in dict_value_of_uuid1 wird die ausgelesene Temperatur eingetragen.
        dict_value_of_uuid1[1].append(timestamp)
        #In die zweite Liste wird der angegebene timestamp eingetragen.
        data.update({"1efa1b5d-ab3f-67cf-9568-314724ae7a79":  dict_value_of_uuid1})
        #Das dict data wird geupdated mit dict_value_of_uuid1 für den key, die UUID Sensor1.
    
    elif uuid == "1efa1b67-d47d-68b1-a923-2c37ef35477a":
    #Ist die gegebene uuid gleich der von Thermometer 2 wird das Folgende ausgeführt:   
        dict_value_of_uuid2 = data["1efa1b67-d47d-68b1-a923-2c37ef35477a"]
        #Dictionary für UUID von Thermometer 2 wird in dict_value_of_uuid2 gespeichert.
        dict_value_of_uuid2[0].append(temp)
        #In die erste Liste in dict_value_of_uuid2 wird die ausgelesene Temperatur eingetragen.
        dict_value_of_uuid2[1].append(timestamp)
        #In die zweite Liste wird der angegebene timestamp eingetragen.
        data.update({"1efa1b67-d47d-68b1-a923-2c37ef35477a":  dict_value_of_uuid2})
        #Das dict data wird geupdated mit dict_value_of_uuid2 für den key, die UUID Sensor2.
   
    else:
    #Ist die gegebene uuid weder die von Thermometer 1, noch 2 wird das Folgende ausgeführt:   
        return "Wrong UUID!"
        
    return data
    #Rückgabe des geupdateten data.

def add_file_metadata(
    file: h5.File, folder_path: str, metadata: Dict[str, Any]
) -> None:
    """Add some basic metadata of the measurement to the file level of an HDF5 file.

    The function utilizes the metadata dictionary and the data from the `group.json`
    data sheet. To access the `group.json` data sheet, it extracts the JSON file's UUID
    from the metadata dictionary. It uses the function `get_json_entry_by_uuid` in order
    to access and add information about the experiment as metadata (with specific keys),
    as follows: the time of creation (`created`), the type of experiment (`experiment`),
    the group number (`group_number`) and the authors (`authors`).

    Args:
        file (h5.File): Object representing an HDF5 file.
        folder_path (str): Path to data sheets folder.
        metadata (Dict[str,Any]): Metadata dictionary.

    """
    group_uuid = metadata["group_info"]["values"]
    #Die UUID von group_info, in metadata, wird aufgerufen und gespeichert in einer Liste group_uuid.
    
    group = m_json.get_json_entry_by_uuid("/home/pi/calorimetry_home/datasheets", group_uuid[0], ["group"])
    #Auslesen des Inhaltes von group in group_info und speichern in dict group.
    file.attrs["created"] = group["created"]
    file.attrs["experiment"] = group["experiment"]
    file.attrs["group_number"] = group["group_number"]
    file.attrs["authors"] = group["authors"]
    #Dem HDF5 File werden die Attribute created, experiment, group_number und authors, mit den jeweiligen Werten aus der JSON Datei übergeben.
    
    
    

def add_data_in_to_file(
    file: h5.File, data: Dict[str, List[List]], metadata: Dict[str, Any]
) -> None:
    """Save the measurement data along with the metadata in an HDF5 file.

    The function creates groups with the UUID of each sensor as the name and adds the
    metadata about its serial number (`serial`) and name (`name`). The measurement data
    and timestamp saved in the data structure are stored in data sets of each group. The
    units (`unit`) of the measurement data and timestamp are stored as metadata of the
    data sets.

    Args:
        file (h5.File): Object representing an HDF5 file.
        data (Dict[str, List[List]]): Data structure.
        metadata (Dict[str, Any]): Metadata dictionary.

    """
    # FIXME: Metadaten in der Aufgabestellung betonnen.
    i = 0 #Zählzeit i
    for uuid in data.keys():
        #For Schleife für alle UUIDs in data.
        
        sensor_group = file.create_group(uuid)
        #Gruppe sensor_group wird für jeweilige uuid erstellt.
        sensor_group[uuid].attrs["serial"] = metadata["sensor"]["serials"][i]
        sensor_group[uuid].attrs["name"] = metadata["sensor"]["names"][i]
        #Die Semsorgruppen erhalten die Attribute serial und name mit den Seriennummern und Namen des jeweiligen Sensors.
        measurements = data[uuid]
        #Messungen für den entsprechenden Sensor werden gesepeichert.
        temperature_group = sensor_group.create_group("temperature")
        temperature_dataset = temperature_group.create_dataset("temperature", data = measurements[0])
        temperature_dataset.attrs["unit"] = "Celsius"
        timestamp_group = sensor_group.create_group("timestamp")
        timestamp_dataset = timestamp_group.create_dataset("timestamps", data = measurements[1])
        timestamp_dataset.attrs["unit"] = "seconds"
        #Eine Untergruppe für Temperatur und für Timestamp wird erstellt.
        #In ihnen Datensätze mit den jeweiligen Temperatur-/Zeitmessungen.
        #Sie bekommen zudem die Attribute unit mit der jeweilifgen Einheit Celsius, bzw. Sekunde übergeben.
        i = i+1
        #Zählzeit +1
    #!!!! Nicht wundern beim öffnen der HDF5 Dateien. Ich habe zunächst nicht gefunden wo die Attribute sind. Deshalb habe ich stattdessen sie in Datensätze mit diesen hinzugefügt und mit diesem Code die Versuche gemacht. Als mir das am Schluss dann auffiel, wollte ich die Versuche nicht noch einmal komplett neu machen. Deshalb stehen die Einheiten, Seriennummern und Namen nicht als Attribut, sondern als Datensätze in den Dateien. Ich habe den Code danach jedoch nochmal geändert.


def get_temperature(sensor: W1ThermSensor) -> float:
    """Retrieves the temperature from a DS18B20 sensor.

    This function also handles the SensorNotReadyError exception by waiting for a
    certain period of time.

    Args:
        sensor (W1ThermSensor): The sensor object to read the temperature from.

    Returns:
        float: Temperature reading.

    """
    sleep_time = 0.05

    counter = 0
    is_data = False
    while not is_data:
        try:
            temperature = sensor.get_temperature()
            is_data = True
        except SensorNotReadyError:
            time.sleep(sleep_time)
            counter += 1
            if counter > 100:
                raise
    return temperature


def get_meas_data_calorimetry(
    metadata: Dict[str, Any], duration: float
) -> Dict[str, List[List]]:
    """Collects and returns temperature measurement data from DS18B20 sensors based on
    the provided metadata.

    This function initializes sensor objects based on the metadata, and continually
    reads temperature values until duration reached. It logs the temperatures and the
    corresponding timestamps.

    Args:
        metadata (Dict[str, Any]): Metadata dictionary.
        duration (float): The duration of the experiment in seconds.

    Returns:
        Dict[str, List[List]]: A dictionary with sensor uuid as keys, and corresponding
            lists of temperatures and timestamps as values.

    Example:
        Input metadata:
        {
            "sensor": {
                "values": ["sensor_uuid_0", "sensor_uuid_1"],
                "serials": ["000005888445", "000005888446"],
                "names": ["FrontSensor", "BackSensor"]
            }
        }

        Output (example data after interruption):
        {
            "sensor_uuid_1": [
                [22, 25, 28], # A list of temperature
                [0, 3, 6], # A list of timestamp
            ],
            "sensor_uuid_2": [
                [22, 25, 28],
                [2, 4, 7],
            ],
        }

    """
    data = {}
    names = []
    sensors = []
    for i, uuid in enumerate(metadata["sensor"]["values"]):
        uuid = metadata["sensor"]["values"][i]
        serial = metadata["sensor"]["serials"][i]

        names.append(metadata["sensor"]["names"][i])
        sensors.append(W1ThermSensor(Sensor.DS18B20, serial))
        data.update({uuid: [[], []]})

    start_time = time.time()
    is_running = True
    while is_running:
        for i, uuid in enumerate(data):
            time_difference = time.time() - start_time
            data = add_experiment_data(sensors[i], uuid, data, time_difference)
            if time_difference >= duration:
                is_running = False
                break
            print(
                "Timestamp: {}, Temperature: {} [{}]".format(
                    data[uuid][1][-1],
                    data[uuid][0][-1],
                    names[i],
                )
            )
        print()

    print(json.dumps(data, indent=4))
    # Ensure that the lengths of temperature and timestamp lists are the same for each
    # sensor and save the time elapsed instead of the absolute timestep.
    for i in data:
        if len(data[i][0]) > len(data[i][1]):
            data[i][0] = data[i][0][0 : len(data[i][1])]
        elif len(data[i][0]) < len(data[i][1]):
            data[i][1] = data[i][1][0 : len(data[i][0])]

    return data


def logging_calorimetry(
    data: Dict[str, List[List]],
    metadata: Dict[str, Any],
    data_folder: str,
    json_folder: str,
) -> str:
    """Logs the calorimetry measurement data into an HDF5 file.

    This function creates a folder (if it does not already exists) and an HDF5 file with
    a specific structure. The data from the provided dictionaries are written to the
    HDF5 file, along with several attributes.

    Args:
        data (Dict[str, List[List]]): Data structure.
        metadata (Dict[str, Any]): Metadata dictionary.
        data_folder (str): Path to the folder where the HDF5 file should be created.
        json_folder (str): Path to the folder containing the data sheets.

    Returns:
        str: Path to the created HDF5 file.

    """
    try:
        # Extract the folder name from the provided path to be used as the HDF5 file
        # name.
        log_name = data_folder.split("/")[-1]
        dataset_path = "{}/{}.hdf5".format(data_folder, log_name)
        if not os.path.exists(data_folder):
            os.makedirs(data_folder)
    except BaseException:
        home_folder = os.path.expanduser("~")
        data_folder = "{}/calcodie_data/{}".format(
            home_folder, time.asctime(time.localtime()).replace(" ", "_")
        )
        if not os.path.exists(data_folder):
            os.makedirs(data_folder)

        dataset_path = "{}/{}.hdf5".format(data_folder, "data")
        print(
            "[WARNING] Something goes wrong, data will be saved in {}.\n".format(
                data_folder
            )
        )

    f = h5.File(dataset_path, "w")
    f.create_group("RawData")

    add_file_metadata(f, json_folder, metadata)
    add_data_in_to_file(f, data, metadata)

    f.close()

