# Lerneinheit II-I: Kalorimetrie (Küchentisch)
## Einführung
Siehe Skript und Aufgabenstellung in [moodle](https://moodle.tu-darmstadt.de/course/view.php?id=36368&section=5#tabs-tree-start).

## Materialien
In diesem GitLab Repositorium finden Sie:
- Package functions (`functions/`): Beinhaltet die Module `m_json`, `m_pck` und Skript `s_uuid6`
- Modul `m_json` (`functions/m_json.py`): Vorlage der Funktionen zum Auslesen, Archivieren und zur Bearbeitung der Metadaten
- Modul `m_pck` (`functions/m_pck.py`): Vorlage der Funktionen zum Aufnahmen uns Abspeichern der Messungsdaten
- Skript `s_uuid6` (`functions/s_uuid6.py`): Um uuid zu generieren
- Python-Hilfsdatei (`functions/__init__.py`): Notwendige Datei für die Erzeugung eines Python-Pakets
- Datenblätter (`datasheets/`): Vorlage der Datenblätter der Komponenten für Küchentischversuch im JSON-Format
- Messdaten (`data/`): Beinhaltet die Ordner der Messdaten
- Abbildungsordner (`figures/`): Ordner zum Ablegen die Fotos bzw. Abbildungen aus der Auswertung
- Notebook Kapazität (`ausarbeitung_kapazitaet.ipynb`): Vorlage zur Messdatenauswertung für Kapazität
- Notebook Newtonsches Abkühlungsgesetz (`ausarbeitung_newton.ipynb`): Vorlage zur Messdatenauswertung für das Newtonsche Abkühlungsgesetz
- Main Skript (`main.py`): Vorlage zum Aufnahmen der Messdaten
- Infosblatt (`asset/DS18B20_Datasheet.pdf`): Infosblatt des DS18B20 Sensors
- Matplotlib Style (`asset/FST.mplstyle`): Einstellung für Matplotlib nach der FST-Institut-Vorschrift
- Readme (`REAMDE.md`): diese Datei
- pyproject.toml (`pyproject.toml`): Metadaten des Projekts
- Requirements (`requirements.txt`): Beschreibt die pip-Umgebung, nicht relevant für die Ausarbeitung

### Links
[h5py Quick Start Guide](https://docs.h5py.org/en/stable/quick.html)

[NumPy Fundamentals](https://numpy.org/doc/stable/user/basics.html)

[Matplotlib Pyplot errorbar](https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.errorbar.html#matplotlib.axes.Axes.errorbar)

[PyPi: W1ThermSensor](https://pypi.org/project/w1thermsensor/)

## Ausarbeitung
Die Ausarbeitung erfolgt in den Modulen und Notebooks. In diesen ist bereits eine Gliederung vorgegeben.

## Abgabe
Die Abgabe erfolgt über [moodle](https://moodle.tu-darmstadt.de/mod/assign/view.php?id=1398099). Committen und pushen Sie zunächst Ihre Änderungen auf GitLab und laden Sie von dort Ihr gesamtes Repo als .zip-Datei herunter (ein direkter Download vom JupyterHub ist leider nicht möglich). Benennen Sie die .zip-Datei nach dem folgenden Schema:

<p style="text-align: center;"> &lt;Nachname&gt;_&lt;Vorname&gt;_&lt;MATR-NR&gt;_&lt;GRUPPEN-NR&gt;_le_2-1.zip</p>

Abgaben, die diese Namenskonvention nicht erfüllen, können in der Bewertung nicht berücksichtigt werden.
Laden Sie diese .zip-Datei in moodle hoch. Insbesondere sollten vorhanden sein:
- Jupyter Notebooks mit Datenauswertungen
- Python-Module
