from functions import m_json
from functions import m_pck
import json
import os
import time
from typing import Any, Dict, List

import h5py as h5
import numpy as np
from w1thermsensor import Sensor, W1ThermSensor
from w1thermsensor.errors import SensorNotReadyError

#Versuch 1: Kapazität

path = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
#Pfad für setup_heat_capacity wird Variable path übergeben.

metadata = m_json.get_metadata_from_setup(path)
#Ausführen der Funktion get_metadata_from_setup mit dem gespeicherten Pfad. Ausgabe: dict mit zusätzlichen Daten für "sensor", welches in Variable metadata gespeichert wird.

metadata = m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets", metadata)
#Ausführen der Funktion add_temperature_sensor_serials. Eingabe: Pfad der datasheets und die vorrige Ergebnis gespeichert in metadata. Ausgabe: dict. Wird wiederum in aktualisierter Variable metadata gespeichert.

time_temp = m_pck.get_meas_data_calorimetry(metadata, 180)
#Ausführen der Funktion get_meas_data_calorimetry. Eingabe: metadata und eine Zeitspanne, hier 180s. Ausgabe: dict mit UUIDs und Temperaturen mit zugeordnetem Zeitstempel, gespeichert in Variable time_temp.


m_pck.logging_calorimetry(time_temp, metadata, "/home/pi/calorimetry_home/data", "/home/pi/calorimetry_home/datasheets")
m_json.archiv_json("/home/pi/calorimetry_home/datasheets", path, "/home/pi/calorimetry_home/data")
#Speichern der Daten


#Versuch 2: Newton

metadata2 = m_json.get_metadata_from_setup("/home/pi/calorimetry_home/datasheets/setup_newton.json")
#Ausführen der Funktion get_metadata_from_setup mit dem gespeicherten Pfad. Ausgabe: dict mit zusätzlichen Daten für "sensor", welches in Variable metadata gespeichert wird.

metadata2 = m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets", metadata2)
#Ausführen der Funktion add_temperature_sensor_serials. Eingabe: Pfad der datasheets und die vorrige Ergebnis gespeichert in metadata. Ausgabe: dict. Wird wiederum in aktualisierter Variable metadata gespeichert.

time_temp2 = m_pck.get_meas_data_calorimetry(metadata2, 600)
#Ausführen der Funktion get_meas_data_calorimetry. Eingabe: metadata und eine Zeitspanne, hier 180s. Ausgabe: dict mit UUIDs und Temperaturen mit zugeordnetem Zeitstempel, gespeichert in Variable time_temp.

m_pck.logging_calorimetry(time_temp2, metadata2, "/home/pi/calorimetry_home/data/data_newton", "/home/pi/calorimetry_home/datasheets")
m_json.archiv_json("/home/pi/calorimetry_home/datasheets", "/home/pi/calorimetry_home/datasheets/setup_newton.json", "/home/pi/calorimetry_home/data/data_newton")
#Speichern der Daten